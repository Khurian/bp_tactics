<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

abstract class AbstractRepository
{
    protected Model $model;

    public function __construct()
    {
        $this->makeModel();
    }

    public function makeModel(): void
    {
        $model = App::make($this->model());

        $this->model = $model;
    }

    abstract public function model(): string;

    public function findById(int $id): ?Model
    {
        return $this->model->find($id);
    }

    public function findLike(string $column, string $value): Collection
    {
        return $this->model->where($column, 'like', '%' . $value.'%')->get();
    }

    public function getAll(): Collection
    {
        return $this->model->all();
    }

    public function save(array $saveData): Model
    {
        return $this->model->create($saveData);
    }

    public function insert(array $saveData): bool
    {
        return $this->model->insert($saveData);
    }

    public function update(int $id, array $updateData): void
    {
        $this->model->where('id', $id)->update($updateData);
    }

    public function remove(int $id): void
    {
        $this->model->where('id',$id)->delete();
    }
}
