<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Tactic;

class TacticRepository extends AbstractRepository
{
    public function model(): string
    {
        return Tactic::class;
    }
}
