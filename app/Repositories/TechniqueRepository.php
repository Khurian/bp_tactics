<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Technique;

class TechniqueRepository extends AbstractRepository
{
    public function model(): string
    {
        return Technique::class;
    }
}
