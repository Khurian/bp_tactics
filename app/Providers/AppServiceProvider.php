<?php

namespace App\Providers;

use App\Console\Commands\SaveTactics;
use App\Mappers\MitreTacticMapper;
use App\Repositories\TacticRepository;
use App\Repositories\TechniqueRepository;
use App\Services\Providers\MitreTacticProvider;
use App\Services\Providers\Search\SearchTechniqueProvider;
use App\Services\Providers\TacticProvider;
use App\Services\TechniqueService;
use App\ViewComposers\TacticsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SaveTactics::class, function ($app) {
            return new SaveTactics(
                $app->get(MitreTacticProvider::class),
                new MitreTacticMapper(),
                new TacticProvider(),
                $app->get(TacticRepository::class),
                $app->get(TechniqueRepository::class)
            );
        });

        $this->app->bind(TechniqueService::class, function ($app) {
            return new TechniqueService(
                $app->make(SearchTechniqueProvider::class
                )
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'layout.leftSider',
            TacticsComposer::class
        );
    }
}
