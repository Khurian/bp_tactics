<?php

declare(strict_types=1);

namespace App\Services\Api;

use App\CQRS\CommandBus;
use App\CQRS\Read\Technique\GetAllTechniquesCommand;
use App\CQRS\Read\Technique\GetAllTechniquesCommandHandler;
use App\CQRS\Read\Technique\GetTechniqueCommand;
use App\CQRS\Read\Technique\GetTechniqueCommandHandler;

final class TechniqueService
{
    public function getAll(): array
    {
        $commandBus = new CommandBus(GetAllTechniquesCommandHandler::class);
        $getAllTechniquesCommand = new GetAllTechniquesCommand();

        return $commandBus->handle($getAllTechniquesCommand);
    }

    public function getOneById(int $id): array
    {
        $commandBus = new CommandBus(GetTechniqueCommandHandler::class);
        $getTechniqueCommand = new GetTechniqueCommand($id);
        $technique = $commandBus->handle($getTechniqueCommand);

        return null === $technique ? [] : $technique->toArray();
    }
}
