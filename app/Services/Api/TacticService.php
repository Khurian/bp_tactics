<?php

declare(strict_types=1);

namespace App\Services\Api;

use App\CQRS\CommandBus;
use App\CQRS\Read\Tactic\GetAllTacticsCommand;
use App\CQRS\Read\Tactic\GetAllTacticsCommandHandler;
use Illuminate\Database\Eloquent\Collection;

final class TacticService
{
    public function getAll(): Collection
    {
        $commandBus = new CommandBus(GetAllTacticsCommandHandler::class);
        $getAllTacticsCommand = new GetAllTacticsCommand();

        return $commandBus->handle($getAllTacticsCommand);
    }
}
