<?php

declare(strict_types=1);

namespace App\Services;

use App\Enums\Time;
use App\Repositories\TacticRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

final class TacticService
{
    private const TACTICS_CACHE = 'all_tactics_cache';

    private TacticRepository $tacticRepository;

    public function __construct(TacticRepository $tacticRepository)
    {
        $this->tacticRepository = $tacticRepository;
    }

    public function getAll(): Collection
    {
        return Cache::remember(
            self::TACTICS_CACHE,
            Time::TEN_MINUTES,
            function () {
                return $this->tacticRepository->getAll();
            }
        );
    }
}
