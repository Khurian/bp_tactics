<?php

declare(strict_types=1);

namespace App\Services;

use App\Services\Providers\Search\SearchTechniqueInterface;
use Illuminate\Database\Eloquent\Collection;

final class TechniqueService
{
    private SearchTechniqueInterface $search;

    public function __construct(SearchTechniqueInterface $search)
    {
        $this->search = $search;
    }

    public function searchByQueryString(string $queryString): Collection
    {
        return $this->search->findByQueryString($queryString);
    }
}
