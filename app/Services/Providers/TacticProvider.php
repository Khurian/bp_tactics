<?php

declare(strict_types=1);

namespace App\Services\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TacticProvider implements TacticWithTechniquesProviderInterface
{
    private const CHUNK = 200;

    public function getAllWithTechniques(): array
    {
        $tacticsWithTechniques = [];

        DB::table('tactics')
            ->select(
                DB::raw(
                    '
                    tactics.id as id,
                    tactics.name as name,
                    tactics.description as description,
                    techniques.id as technique_id,
                    techniques.name as technique_name,
                    techniques.created as technique_created,
                    techniques.description as technique_description,
                    techniques.tactic_id as technique_tactic_id
                '
                )
            )
            ->orderBy('tactics.id')
            ->leftJoin('techniques', 'tactics.id', '=', 'techniques.tactic_id')
            ->chunk(
                self::CHUNK,
                function (Collection $tactics) use (&$tacticsWithTechniques) {
                    $this->buildTacticsWithTechniques($tactics, $tacticsWithTechniques);
                }
            );

        return $tacticsWithTechniques;
    }

    private function buildTacticsWithTechniques(Collection $tactics, array &$tacticsWithTechniques): void
    {
        foreach ($tactics as $tactic) {
            $tactic = (array) $tactic;

            if (isset($tacticsWithTechniques[$tactic['name']])) {
                $tacticsWithTechniques[$tactic['name']]['techniques'][$this->createTechniqueKey($tactic)]
                    = $this->buildTechniqueArray($tactic);

                continue;
            }

            $tacticsWithTechniques[$tactic['name']] = [
                'id' => $tactic['id'],
                'name' => $tactic['name'],
                'description' => $tactic['description'],
                'techniques' => [
                    $this->createTechniqueKey($tactic) => $this->buildTechniqueArray($tactic)
                ]
            ];
        }
    }

    private function buildTechniqueArray(array $technique): array
    {
        return [
            'id' =>  $technique['technique_id'],
            'name' => $technique['technique_name'],
            'description' => $technique['technique_description'],
            'tactic_id' => $technique['technique_tactic_id'],
            'created' => $technique['technique_created']
        ];
    }

    private function createTechniqueKey(array $tactic): string
    {
        return $tactic['name'] . '_' . $tactic['technique_name'] . '_' . $tactic['technique_created'];
    }
}
