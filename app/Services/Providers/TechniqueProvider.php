<?php

declare(strict_types=1);

namespace App\Services\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

final class TechniqueProvider
{
    private const CHUNK = 500;

    public function getAll(): array
    {
        $techniques = [];

        DB::table('techniques')
            ->select([
                'id',
                'name',
                'description',
                'tactic_id'
            ])
            ->orderBy('id')
            ->chunk(self::CHUNK, function (Collection $techniquesPart) use (&$techniques) {
                $techniques = array_merge($techniques, $techniquesPart->toArray());
            });

        return $techniques;
    }
}
