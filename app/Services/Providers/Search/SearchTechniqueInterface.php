<?php

declare(strict_types=1);

namespace App\Services\Providers\Search;

use Illuminate\Database\Eloquent\Collection;

interface SearchTechniqueInterface
{
    public function findByQueryString(string $queryString): Collection;
}
