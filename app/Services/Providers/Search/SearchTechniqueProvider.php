<?php

declare(strict_types=1);

namespace App\Services\Providers\Search;

use App\Repositories\TechniqueRepository;
use Illuminate\Database\Eloquent\Collection;

final class SearchTechniqueProvider implements SearchTechniqueInterface
{
    private const SEARCH_COLUMN = 'name';

    private TechniqueRepository $techniqueRepository;

    public function __construct(TechniqueRepository $techniqueRepository)
    {
        $this->techniqueRepository = $techniqueRepository;
    }

    public function findByQueryString(string $queryString): Collection
    {
        return $this->techniqueRepository->findLike(self::SEARCH_COLUMN, $queryString);
    }
}
