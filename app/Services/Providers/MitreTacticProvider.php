<?php

declare(strict_types=1);

namespace App\Services\Providers;

use GuzzleHttp\Client;

class MitreTacticProvider implements WebProviderInterface
{
    private const URL = 'https://raw.githubusercontent.com/mitre/cti/master/enterprise-attack/enterprise-attack.json';

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get(): array
    {
        $response = $this->client->get(self::URL);

        return json_decode($response->getBody()->getContents(), true);
    }
}
