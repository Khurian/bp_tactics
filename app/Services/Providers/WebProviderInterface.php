<?php

declare(strict_types=1);

namespace App\Services\Providers;

interface WebProviderInterface
{
    public function get(): array;
}
