<?php

declare(strict_types=1);

namespace App\Services\Providers;

interface TacticWithTechniquesProviderInterface
{
    public function getAllWithTechniques(): array;
}
