<?php

declare(strict_types=1);

namespace App\Mappers;

use App\Exceptions\InvalidResponseDataException;

final class MitreTacticMapper implements RelationshipMapperInterface
{
    private const NO_NAME = 'no_name';
    private const NO_DESC = 'no_desc';

    public function mapWithRelationship(array $response): array
    {
        if (false === isset($response['objects'])) {
            throw new InvalidResponseDataException('object field not exist');
        }

        return $this->createArrayWithRelationships($response['objects']);
    }

    private function createArrayWithRelationships(array $response): array
    {
        $tactics = $this->getSortedByTacticsAndTechniques($response);
        $tactics = $this->buildRelationships($tactics);

        return $tactics['tactics'];
    }

    private function getSortedByTacticsAndTechniques(array $objects): array
    {
        $tactics['tactics'] = [];
        $tactics['techniques'] = [];

        foreach ($objects as $object) {
            if (isset($object['x_mitre_shortname'])) {
                $tactics['tactics'][$object['x_mitre_shortname']] = $this->buildTacticArray($object);
                continue;
            }

            $tactics['techniques'][] = $object;
        }

        return $tactics;
    }

    private function buildRelationships(array $tactics): array
    {
        foreach ($tactics['techniques'] as $technique) {
            if (false === isset($technique['kill_chain_phases'])) {
                continue;
            }

            $tactics = $this->buildRelationship($tactics, $technique);
        }

        return $tactics;
    }

    private function buildRelationship(array $tactics, array $technique): array
    {
        foreach ($technique['kill_chain_phases'] as $phase) {
            $tactics['tactics'][$phase['phase_name']]['techniques'][] = $this->buildTechniqueArray($technique);
        }

        return $tactics;
    }

    private function buildTacticArray(array $tactic): array
    {
        return [
            'name' => $tactic['name'] ?? self::NO_NAME,
            'description' => $tactic['description'] ?? self::NO_DESC
        ];
    }

    private function buildTechniqueArray(array $technique): array
    {
        return [
            'name' => $technique['name'] ?? self::NO_NAME,
            'description' => $technique['description'] ?? self::NO_DESC,
            'created' => $technique['created']
        ];
    }
}
