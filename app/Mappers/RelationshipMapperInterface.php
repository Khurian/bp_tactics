<?php

declare(strict_types=1);

namespace App\Mappers;

interface RelationshipMapperInterface
{
    public function mapWithRelationship(array $responeArray): array;
}
