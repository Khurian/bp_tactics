<?php

declare(strict_types=1);

namespace App\CQRS;

use Illuminate\Support\Facades\App;

final class CommandBus
{
    private string $commandHandlerNamespace;

    public function __construct(string $commandHandlerNamespace)
    {
        $this->commandHandlerNamespace = $commandHandlerNamespace;
    }

    public function handle(object $saveCommand): mixed
    {
        return App::make($this->commandHandlerNamespace)->handle($saveCommand);
    }
}
