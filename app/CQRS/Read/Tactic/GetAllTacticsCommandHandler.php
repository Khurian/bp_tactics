<?php

declare(strict_types=1);

namespace App\CQRS\Read\Tactic;

use App\Enums\Time;
use App\Repositories\TacticRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

final class GetAllTacticsCommandHandler
{
    private const API_TACTICS_CACHE = 'api_all_tactics_cache';

    private TacticRepository $tacticRepository;

    public function __construct(TacticRepository $tacticRepository)
    {
        $this->tacticRepository = $tacticRepository;
    }

    public function handle(GetAllTacticsCommand $command): Collection
    {
        return Cache::remember(
            self::API_TACTICS_CACHE,
            Time::TEN_MINUTES,
            function () {
                return $this->tacticRepository->getAll();
            }
        );
    }
}
