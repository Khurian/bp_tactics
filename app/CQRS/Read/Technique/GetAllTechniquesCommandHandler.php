<?php

declare(strict_types=1);

namespace App\CQRS\Read\Technique;

use App\Enums\Time;
use App\Services\Providers\TechniqueProvider;
use Illuminate\Support\Facades\Cache;

final class GetAllTechniquesCommandHandler
{
    private const API_TECHNIQUES_CACHE = 'api_all_techniques_cache';

    private TechniqueProvider $techniqueProvider;

    public function __construct(TechniqueProvider $techniqueProvider)
    {
        $this->techniqueProvider = $techniqueProvider;
    }

    public function handle(GetAllTechniquesCommand $command): array
    {
        return Cache::remember(
            self::API_TECHNIQUES_CACHE,
            Time::TEN_MINUTES,
            function () {
                return $this->techniqueProvider->getAll();
            }
        );
    }
}
