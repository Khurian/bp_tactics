<?php

declare(strict_types=1);

namespace App\CQRS\Read\Technique;

final class GetTechniqueCommand
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }
}
