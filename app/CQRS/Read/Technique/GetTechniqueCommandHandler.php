<?php

declare(strict_types=1);

namespace App\CQRS\Read\Technique;

use App\Repositories\TechniqueRepository;
use Illuminate\Database\Eloquent\Model;

final class GetTechniqueCommandHandler
{
    private TechniqueRepository $techniqueRepository;

    public function __construct(TechniqueRepository $techniqueRepository)
    {
        $this->techniqueRepository = $techniqueRepository;
    }

    public function handle(GetTechniqueCommand $command): ?Model
    {
        return $this->techniqueRepository->findById($command->id());
    }
}
