<?php

declare(strict_types=1);

namespace App\Enums;

final class Time
{
    public const TEN_MINUTES = 600;
}
