<?php

declare(strict_types=1);

namespace App\ViewComposers;

use Illuminate\View\View;
use App\Services\TacticService;

final class TacticsComposer
{
    private TacticService $tacticService;

    public function __construct(TacticService $tacticService)
    {
        $this->tacticService = $tacticService;
    }

    public function compose(View $view): void
    {
        $view->with(
            'tactics',
            $this->tacticService->getAll()
        );
    }
}
