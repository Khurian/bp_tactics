<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\TacticRepository;
use Illuminate\Contracts\View\View;

final class TacticController
{
    private TacticRepository $tacticRepository;

    public function __construct(TacticRepository $tacticRepository)
    {
        $this->tacticRepository = $tacticRepository;
    }

    public function index(): View
    {
        return view('pages.tactics');
    }

    public function show(int $id): View
    {
        $tactic = $this->tacticRepository->findById($id);

        return view('pages.tactic', compact('tactic'));
    }
}
