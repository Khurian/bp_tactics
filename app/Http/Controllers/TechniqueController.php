<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\TechniqueSearchRequest;
use App\Repositories\TechniqueRepository;
use App\Services\TechniqueService;
use Illuminate\Contracts\View\View;

final class TechniqueController
{
    private TechniqueRepository $techniqueRepository;

    private TechniqueService $techniqueService;

    public function __construct(
        TechniqueRepository $techniqueRepository,
        TechniqueService $techniqueService
    )
    {
        $this->techniqueRepository = $techniqueRepository;
        $this->techniqueService = $techniqueService;
    }

    public function show(int $id): View
    {
        $technique = $this->techniqueRepository->findById($id);

        return view('pages.technique', compact('technique'));
    }

    public function search(TechniqueSearchRequest $request): View
    {
        $techniques = $this->techniqueService->searchByQueryString($request['queryString']);

        return view('pages.techniques', compact('techniques'));
    }
}
