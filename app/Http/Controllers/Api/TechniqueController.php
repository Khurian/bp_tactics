<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Services\Api\TechniqueService;
use Symfony\Component\HttpFoundation\JsonResponse;

final class TechniqueController
{
    private TechniqueService $techniqueService;

    public function __construct(TechniqueService $techniqueService)
    {
        $this->techniqueService = $techniqueService;
    }

    public function index(): JsonResponse
    {
        return response()->json([
            'techniques' => $this->techniqueService->getAll()
        ]);
    }

    public function show(int $id): JsonResponse
    {
        return response()->json([
            'technique' => $this->techniqueService->getOneById($id)
        ]);
    }
}
