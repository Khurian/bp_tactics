<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Services\Api\TacticService;
use Symfony\Component\HttpFoundation\JsonResponse;

final class TacticController
{
    private TacticService $tacticService;

    public function __construct(TacticService $tacticService)
    {
        $this->tacticService = $tacticService;
    }

    public function index(): JsonResponse
    {
        return response()->json([
            'tactics' => $this->tacticService->getAll()->toArray()
        ]);
    }
}
