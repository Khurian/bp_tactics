<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class TechniqueSearchRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'queryString' => 'required|min:2',
        ];
    }
}
