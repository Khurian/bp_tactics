<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\SaveTactics;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

final class Kernel extends ConsoleKernel
{
    /**
     * @var array
     */
    protected $commands = [
        SaveTactics::class
    ];

    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('tactics:save')->daily();
    }

    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
