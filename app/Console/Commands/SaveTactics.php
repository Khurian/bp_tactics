<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Mappers\RelationshipMapperInterface;
use App\Repositories\TacticRepository;
use App\Repositories\TechniqueRepository;
use App\Services\Providers\TacticWithTechniquesProviderInterface;
use App\Services\Providers\WebProviderInterface;
use Illuminate\Console\Command;

final class SaveTactics extends Command
{
    /** @var string */
    protected $signature = 'tactics:save';

    /** @var string */
    protected $description = 'Get tactis data and save';

    private WebProviderInterface $webTacticsProvider;

    private RelationshipMapperInterface $relationshipMapper;

    private TacticWithTechniquesProviderInterface $tacticWithTechniquesProvider;

    private TacticRepository $tacticRepository;

    private TechniqueRepository $techniqueRepository;

    public function __construct(
        WebProviderInterface $webTacticsProvider,
        RelationshipMapperInterface $relationshipMapper,
        TacticWithTechniquesProviderInterface $tacticWithTechniquesProvider,
        TacticRepository $tacticRepository,
        TechniqueRepository $techniqueRepository
    )
    {
        $this->webTacticsProvider = $webTacticsProvider;
        $this->relationshipMapper = $relationshipMapper;
        $this->tacticWithTechniquesProvider = $tacticWithTechniquesProvider;
        $this->tacticRepository = $tacticRepository;
        $this->techniqueRepository = $techniqueRepository;

        parent::__construct();
    }

    public function handle(): void
    {
        $tacticsFromWebsite = $this->getTacticsFromWebsite();
        $tacticWithTechniques = $this->tacticWithTechniquesProvider->getAllWithTechniques();

        foreach ($tacticsFromWebsite as $tactic) {
            if (false === isset($tacticWithTechniques[$tactic['name']])) {
                $this->saveTacticWithTechniques($tactic);
                continue;
            }

            $this->makeOperationOnTechniques($tactic, $tacticWithTechniques[$tactic['name']]);

            if ($tactic['description'] != $tacticWithTechniques[$tactic['name']]['description']) {
                $this->tacticRepository->update(
                    $tacticWithTechniques[$tactic['name']]['id'],
                    [
                        'description' => $tactic['description']
                    ]
                );
            }
        }
    }

    private function getTacticsFromWebsite(): array
    {
        $tactics = $this->webTacticsProvider->get();

        return $this->relationshipMapper->mapWithRelationship($tactics);
    }

    private function saveTacticWithTechniques(array $tactic): void
    {
        $tacticModel = $this->tacticRepository->save([
            'name' => $tactic['name'],
            'description' => $tactic['description']
        ]);

        $techniques = $this->bindTechniquesWithTacticId($tactic['techniques'], $tacticModel->id);

        $this->techniqueRepository->insert($techniques);
    }

    private function bindTechniquesWithTacticId(array $techniques, int $tacticId): array
    {
        $techniquesWithTacticId = [];
        foreach ($techniques as $technique) {
            $techniquesWithTacticId[] = [
                'name' => $technique['name'],
                'description' => $technique['description'],
                'created' => $technique['created'],
                'tactic_id' => $tacticId
            ];
        }

        return $techniquesWithTacticId;
    }

    private function makeOperationOnTechniques(array $tactic, array $tacticWithTechniques): void
    {
        foreach ($tactic['techniques'] as $technique) {
            $keyName = $this->createTechniqueKeyName($tactic['name'], $technique);
            if (false === isset($tacticWithTechniques['techniques'][$keyName])) {
                $this->techniqueRepository->save([
                    'name' => $technique['name'],
                    'description' => $technique['description'],
                    'tactic_id' => $tacticWithTechniques['id']
                ]);

                continue;
            }

            $tacticWithTechnique = $tacticWithTechniques['techniques'][$keyName];
            if ($technique['description'] != $tacticWithTechnique['description']) {
                $this->techniqueRepository->update(
                    $tacticWithTechnique['id'],
                    [
                        'description' => $technique['description']
                    ]
                );
            }
        }
    }

    private function createTechniqueKeyName(string $tacticName, array $technique): string
    {
        return $tacticName . '_' . $technique['name'] . '_' . $technique['created'];
    }
}
