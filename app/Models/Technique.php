<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class Technique extends Model
{
    /** @var string */
    protected $table = 'techniques';

    /** @var bool */
    public $timestamps = false;

    /** @var string[] */
    protected $fillable = [
        'name',
        'description',
        'tactic_id',
        'created'
    ];

    public function tactic(): BelongsTo
    {
        return $this->belongsTo(Tactic::class, 'tactic_id', 'id');
    }
}
