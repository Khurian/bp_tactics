<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Tactic extends Model
{
    /** @var string */
    protected $table = 'tactics';

    /** @var bool */
    public $timestamps = false;

    /** @var string[] */
    protected $fillable = [
        'name',
        'description'
    ];

    public function techniques(): HasMany
    {
        return $this->hasMany(Technique::class, 'tactic_id', 'id');
    }
}
