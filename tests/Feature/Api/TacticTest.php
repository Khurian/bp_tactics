<?php

declare(strict_types=1);

namespace Tests\Feature\Api;

use Tests\TestCase;

final class TacticTest extends TestCase
{
    public function testGetAllEndpoint(): void
    {
        $response = $this->get('/api/tactics');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'tactics' => [
                '*' => [
                    'id',
                    'name',
                    'description'
                ]
            ]
        ]);
    }
}
