<?php

declare(strict_types=1);

namespace Tests\Feature\Api;

use Tests\TestCase;

final class TechniqueTest extends TestCase
{
    public function testGetAllEndpoint(): void
    {
        $response = $this->get('/api/techniques');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'techniques' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'tactic_id'
                ]
            ]
        ]);
    }

    public function testGetOneEndpoint(): void
    {
        $response = $this->get('/api/techniques/1');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'technique' => [
                'id',
                'name',
                'description',
                'tactic_id'
            ]
        ]);
    }

    public function testGetOneEndpointWithStringId(): void
    {
        $response = $this->get('/api/techniques/test');

        $response->assertStatus(404);
    }
}
