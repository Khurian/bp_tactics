<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

final class TechniquePageTest extends TestCase
{
    public function testGetOnePage(): void
    {
        $response = $this->get('/technique/1');

        $response->assertStatus(200);
    }

    public function testGetOnePageWithStringId(): void
    {
        $response = $this->get('/technique/test');

        $response->assertStatus(404);
    }

    public function testSearchPage(): void
    {
        $response = $this->get('/search/technique?queryString=AS');

        $response->assertStatus(200);
    }
}
