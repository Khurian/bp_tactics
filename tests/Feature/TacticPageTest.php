<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

final class TacticPageTest extends TestCase
{
    public function testGetAllPage(): void
    {
        $response = $this->get('/tactic/1');

        $response->assertStatus(200);
    }
}
