<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

final class MainPageTest extends TestCase
{
    public function testMainPage(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
