<?php

declare(strict_types=1);

namespace Tests\Unit\Console;

use App\Console\Commands\SaveTactics;
use App\Mappers\MitreTacticMapper;
use App\Models\Tactic;
use App\Repositories\TacticRepository;
use App\Repositories\TechniqueRepository;
use App\Services\Providers\MitreTacticProvider;
use App\Services\Providers\TacticProvider;
use PHPUnit\Framework\TestCase;

final class SaveTacticsTest extends TestCase
{
    private array $mitreTacticData = [
        'type' => 'bundle',
        'id' => 'bundle--491cea28-0159-45df-b790-c315506e3e8e',
        'spec_version' => '2.0',
        'objects' => [
            [
                'id' => 'attack-pattern--01df3350-ce05-4bdf-bdf8-0a919a66d4a8',
                'name' => 'confluence',
                'description' => 'desc',
                'x_mitre_shortname' => 'collection',
                'created' => 'created'
            ],
            [
                'id' => 'attack-pattern--01df3350-ce05-4bdf-bdf8-0a919a66d4a8',
                'name' => 'some_technique',
                'description' => 'desc_technique',
                'created' => 'created_technique',
                'kill_chain_phases' => [
                    [
                        'phase_name' => 'collection'
                    ]
                ],
            ]
        ]
    ];

    private array $tacticsData = [
        'confluence' => [
            'id' => 1,
            'name' => 'testName',
            'description' => 'old desc',
            'techniques' => [
                'confluence_some_technique_created_technique' => [
                    'id' => 1,
                    'name' => 'some_technique',
                    'description' => 'old technique_desc',
                    'tactic_id' => 1,
                    'created' => 'created_technique'
                ]
            ]
        ]
    ];

    public function testHandleWhenDatabaseTableIsEmpty(): void
    {
        $mockedMitreTacticProvider = $this->createMock(MitreTacticProvider::class);
        $mockedMitreTacticProvider->expects($this->once())->method('get')->willReturn($this->mitreTacticData);

        $mockedTacticProvider = $this->createMock(TacticProvider::class);
        $mockedTacticProvider->expects($this->once())->method('getAllWithTechniques')->willReturn([]);

        $tacticModel = new Tactic();
        $tacticModel->id = 1;
        $mockedTacticRepository = $this->createMock(TacticRepository::class);
        $mockedTacticRepository->expects($this->once())->method('save')->with([
            'name' => 'confluence',
            'description' => 'desc',
        ])->willReturn($tacticModel);

        $mockedTechniqueRepository = $this->createMock(TechniqueRepository::class);
        $mockedTechniqueRepository->expects($this->once())->method('insert')->with(
            [
                [
                    'name' => 'some_technique',
                    'description' => 'desc_technique',
                    'tactic_id' => 1,
                    'created' => 'created_technique'
                ]
            ]
        )->willReturn(true);

        $saveTactics = new SaveTactics(
            $mockedMitreTacticProvider,
            new MitreTacticMapper(),
            $mockedTacticProvider,
            $mockedTacticRepository,
            $mockedTechniqueRepository
        );

        $saveTactics->handle();
    }

    public function testHandleWithUpdate(): void
    {
        $mockedMitreTacticProvider = $this->createMock(MitreTacticProvider::class);
        $mockedMitreTacticProvider->expects($this->once())->method('get')->willReturn($this->mitreTacticData);

        $mockedTacticProvider = $this->createMock(TacticProvider::class);
        $mockedTacticProvider->expects($this->once())
            ->method('getAllWithTechniques')
            ->willReturn($this->tacticsData);

        $mockedTacticRepository = $this->createMock(TacticRepository::class);
        $mockedTacticRepository->expects($this->once())->method('update')->with(
            1,
            [
                'description' => 'desc'
            ]
        );

        $mockedTechniqueRepository = $this->createMock(TechniqueRepository::class);
        $mockedTechniqueRepository->expects($this->once())->method('update')->with(
            1,
            [
                'description' => 'desc_technique'
            ]
        );

        $saveTactics = new SaveTactics(
            $mockedMitreTacticProvider,
            new MitreTacticMapper(),
            $mockedTacticProvider,
            $mockedTacticRepository,
            $mockedTechniqueRepository
        );

        $saveTactics->handle();
    }
}
