<?php

declare(strict_types=1);

namespace Tests\Unit\Mappers;

use App\Exceptions\InvalidResponseDataException;
use App\Mappers\MitreTacticMapper;
use PHPUnit\Framework\TestCase;

final class MitreTacticMapperTest extends TestCase
{
    public function testMapWithRelationship(): void
    {
        $mitreTacticMapper = new MitreTacticMapper();
        $response = [
            'type' => 'bundle',
            'id' => 'bundle--491cea28-0159-45df-b790-c315506e3e8e',
            'spec_version' => '2.0',
            'objects' => [
                [
                    'id' => 'attack-pattern--01df3350-ce05-4bdf-bdf8-0a919a66d4a8',
                    'name' => 'testName',
                    'description' => 'desc',
                    'x_mitre_shortname' => 'collection',
                    'created' => 'created'
                ],
                [
                    'id' => 'attack-pattern--01df3350-ce05-4bdf-bdf8-0a919a66d4a8',
                    'name' => 'testName2',
                    'description' => 'desc2',
                    'x_mitre_shortname' => 'collection2',
                    'created' => 'created2'
                ],
                [
                    'id' => 'attack-pattern--01df3350-ce05-4bdf-bdf8-0a919a66d4a8',
                    'name' => 'testName3',
                    'description' => 'desc3',
                    'x_mitre_shortname' => 'collection3',
                    'created' => 'created3'
                ],
                [
                    'id' => 'attack-pattern--01df3350-ce05-4bdf-bdf8-0a919a66d4a8',
                    'name' => 'testName4',
                    'description' => 'desc4',
                    'created' => 'created4',
                    'kill_chain_phases' => [
                        [
                            'phase_name' => 'collection'
                        ]
                    ],
                ]
            ]
        ];
        $resultShouldBe = [
            'collection' => [
                'name' => 'testName',
                'description' => 'desc',
                'techniques' => [
                    [
                        'name' => 'testName4',
                        'description' => 'desc4',
                        'created' => 'created4',
                    ]
                ]
            ],
            'collection2' => [
                'name' => 'testName2',
                'description' => 'desc2',
            ],
            'collection3' => [
                'name' => 'testName3',
                'description' => 'desc3',
            ]
        ];

        $this->assertEquals($resultShouldBe, $mitreTacticMapper->mapWithRelationship($response));
    }

    public function testMapWithRelationshipByEmptyArray(): void
    {
        $this->expectException(InvalidResponseDataException::class);

        (new MitreTacticMapper())->mapWithRelationship([]);
    }
}
