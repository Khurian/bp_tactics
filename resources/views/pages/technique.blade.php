@extends('layout.main')

@section('content')

    <div class="col-lg-9">

        <div class="card mt-4">
            <div class="card-body">
                <h3 class="card-title">
                    {{ $technique->id }}. {{ $technique->name }}
                </h3>
            </div>
        </div>

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
                Description
            </div>
            <div class="card-body">
                {{ $technique->description }}
            </div>
        </div>
    </div>
@endsection
