@extends('layout.main')

@section('content')

    <div class="col-lg-9">

        <div class="card mt-4">
            <div class="card-body">
                <h3 class="card-title">Techniques</h3>
            </div>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
                Search result(s):
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2"> ID </div>
                    <div class="col-md-8"> Name </div>
                </div>
                @foreach($techniques as $technique)
                    <a href="{{ url('technique/' . $technique->id) }}">
                        <div class="row">
                            <div class="col-md-2">
                                {{ $technique->id }}
                            </div>
                            <div class="col-md-8">
                                {{ $technique->name }}
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

    </div>
@endsection
