@extends('layout.main')

@section('content')

            <div class="col-lg-9">

                <div class="card mt-4">
                    <div class="card-body">
                        <h3 class="card-title">Tactics and Techniques</h3>
                    </div>
                </div>
                <!-- /.card -->

                <div class="card card-outline-secondary my-4">
                    <div class="card-header">
                        Search
                    </div>
                    <div class="card-body">
                        Find technique(s):
                        <form method="GET" action="{{ url('search/technique') }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control" name="queryString">
                                </div>
                                <div class="col-md-4">
                                    <button class="form-control btn-success">Search</button>
                                </div>
                            </div>
                        </form>
                        @if($errors->has('queryString'))
                            <div style="color: red;">{{ $errors->first('queryString') }}</div>
                        @endif
                    </div>
                </div>

                <div class="card card-outline-secondary my-4">
                    <div class="card-header">
                        API
                    </div>
                    <div class="card-body">
                        Endpoints (will open on new tab): <br>
                        <a href="{{ url('api/tactics') }}" target="_blank"> All tactics </a> <br>
                        <a href="{{ url('api/techniques') }}" target="_blank"> All techniques </a> <br>
                        <a href="{{ url('api/techniques/1') }}" target="_blank"> Technique with id = 1</a> <br>
                    </div>
                </div>
            </div>
@endsection
