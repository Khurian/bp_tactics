<div class="col-lg-3">
    <h1 class="my-4">TACTICS</h1>
    <div class="list-group">
        @foreach($tactics as $tactic)
            <a href="{{ url('/tactic/' . $tactic->id) }}" class="list-group-item">
                {{ $tactic->name }}
            </a>
        @endforeach
    </div>
</div>
