<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link href="{{ url('/css/bootstrap.css') }}" rel="stylesheet">
</head>
<body>

    @include('layout.header')
    <div class="container">
        <div class="row">
            @include('layout.leftSider')
            @yield('content')
        </div>
    </div>

    @include('layout.footer')

<script src="{{ url('/js/bootstrap.js') }}"></script>
</body>
</html>
