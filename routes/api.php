<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Api routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your Api!
|
*/

Route::get('/tactics', 'Api\TacticController@index');
Route::get('/techniques', 'Api\TechniqueController@index');
Route::get('/techniques/{id}', 'Api\TechniqueController@show')->where('id', '[0-9]+');
