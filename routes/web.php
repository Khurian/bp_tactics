<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TacticController@index');
Route::get('tactic/{id}', 'TacticController@show')->where('id', '[0-9]+');
Route::get('technique/{id}', 'TechniqueController@show')->where('id', '[0-9]+');
Route::get('search/technique', 'TechniqueController@search');
